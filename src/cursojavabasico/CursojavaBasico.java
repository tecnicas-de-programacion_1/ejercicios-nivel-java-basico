/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package cursojavabasico;

import java.util.Scanner;

public class CursojavaBasico {

    public static void main(String[] args) {
        System.out.println("CURSO BASICO DE PROGRAMACION JAVA");
        System.out.println("\nOpc 1: Ejercicios condicionales");
        System.out.println("Opc 2: Ejercicios de ciclos NIVEL 0");
        System.out.println("Opc 3: Ejecicios de ciclos NIVEL 1\n");

        Scanner sc = new Scanner(System.in);
        System.out.println("Escoga una opcion");
        int opcion = Integer.parseInt(sc.nextLine());
        System.out.println("La opcion escogida es " + opcion);

        switch (opcion) {
            case 1: {
                System.out.println("Usted eligio los ejercicios condicionales\n");
                longitudTriangulo();
                restricciónCarro();
                notaEst();
                medPrisma();
                cafeInter();
                break;
            }
            case 2: {
                numAscend();
                numEnt();
                sumList();
                listMult();
                sumNum();
            }
            break;
            case 3: {
                rerSem();
                promNotas();
                venTotales();
            }
            break;
            default: {
                System.out.println("Usted eligio mal la opcion o no existe");
            }
        }

    }

    public static void longitudTriangulo() {
        int ladoUno, ladoDos, ladoTres;
        Scanner tri = new Scanner(System.in);

        System.out.println("Ejercicio 1");
        System.out.println("Ingrese la longitud de los tres lados del triangulo");
        ladoUno = tri.nextInt();
        ladoDos = tri.nextInt();
        ladoTres = tri.nextInt();

        if (ladoUno == ladoDos & ladoDos == ladoTres) {
            System.out.println("El triangulo es equilatero\n");
        } else if (ladoUno != ladoDos & ladoUno != ladoTres & ladoTres != ladoDos) {
            System.out.println("El triangulo es escaleno\n");
        } else if (ladoUno == ladoDos || ladoUno == ladoTres || ladoDos == ladoTres) {
            System.out.println("El triangulo es isosceles\n");
        }
    }

    public static void restricciónCarro() {
        int dia, cedula, ultimoDigito;
        Scanner car = new Scanner(System.in);

        System.out.println("Ejercicio 2");
        System.out.println("Ingrese el dia del mes (1-31): ");
        dia = car.nextInt();
        System.out.println("Ingrese el numero de cedula: ");
        cedula = car.nextInt();

        ultimoDigito = cedula % 10;

        if ((dia % 2 == 0 && ultimoDigito % 2 == 1) || (dia % 2 == 1 && ultimoDigito % 2 == 0)) {
            System.out.println("No tiene restriccion, puede transitar.\n");
        } else {
            System.out.println("Tiene restriccion, no puede transitar.\n");
        }
    }

    public static void notaEst() {
        double nota;
        Scanner not = new Scanner(System.in);

        System.out.println("Ejercicio 3");
        System.out.println("Ingrese la nota del estudiante");
        nota = not.nextDouble();

        if (nota >= 0 & nota <= 50) {
            System.out.println("La nota es valida\n");
        } else {
            System.out.println("La nota no es valida\n");
        }
    }

    public static void medPrisma() {
        double longBase, anchoBase, h, areaSup, volPrisma;
        Scanner pris = new Scanner(System.in);

        System.out.println("Ejercicio 4");
        System.out.println("Ingrese la longitud (largo) de la base del prisma");
        longBase = pris.nextDouble();
        System.out.println("Ingrese el ancho de la base del prisma");
        anchoBase = pris.nextDouble();
        System.out.println("Ingrese la altura del prisma");
        h = pris.nextDouble();

        areaSup = 2 * (longBase * anchoBase) + 2 * (longBase * h) + 2 * (anchoBase * h);
        volPrisma = longBase * anchoBase * h;

        System.out.println("El area de la superficie del prisma es: " + areaSup
                + " y su volumen es: " + volPrisma + "\n");
    }

    public static void cafeInter() {
        int cantTin, cantArom;
        double ganTin, ganArom;
        Scanner caf = new Scanner(System.in);

        System.out.println("Ejercicio 5");
        System.out.println("Ingrese la cantidad de tintos y aromaticas vendidos");
        cantTin = caf.nextInt();
        cantArom = caf.nextInt();

        ganTin = cantTin * 1200 * 0.25;
        ganArom = cantArom * 1000 * 0.32;

        if (ganTin > ganArom) {
            System.out.println("El producto con mayor ganancia son los tintos");
            System.out.println("La ganancia fue de $" + ganTin);
            System.out.println("La diferencia con las aromaticas es de $" + (ganTin - ganArom) + "\n");
        } else if (ganArom > ganTin) {
            System.out.println("El producto con mayor ganancia son las aromaticas");
            System.out.println("La ganancia fue de $" + ganArom);
            System.out.println("La diferencia con las aromaticas es de $" + (ganArom - ganTin) + "\n");
        }
    }

    public static void numAscend() {
        System.out.println("Ejercicio 1");
        System.out.println("Orden de los numeros del 15 al 32\n");
        for (int i = 15; i <= 32; i++) {
            System.out.println("El orden en ascendencia entre 15 y 32 es: " + i);
        }
        System.out.println(" ");
        for (int i = 32; i >= 15; i--) {
            System.out.println("El orden en descendencia entre 15 y 32 es: " + i);
        }
    }

    public static void numEnt() {
        int a, b;
        Scanner num = new Scanner(System.in);

        System.out.println("\nEjercicio 2");
        System.out.println("Ingrese los valores enteros correspondiente a \"A\" y \"B\"");
        a = num.nextInt();
        b = num.nextInt();
        for (int i = a; i <= b; i++) {
            System.out.println("El orden en ascendencia entre \"A\" y \"B\" es: " + i);
        }
        System.out.println("");
        for (int i = b; i >= a; i--) {
            System.out.println("El orden en descendencia entre \"A\" y \"B\" es: " + i);
        }
    }

    public static void sumList() {
        int a, b, conSum = 0;
        Scanner num2 = new Scanner(System.in);

        System.out.println("\nEjercicio 3");
        System.out.println("Ingrese los valores enteros correspondiente a \"A\" y \"B\"");
        a = num2.nextInt();
        b = num2.nextInt();
        for (int i = a; i <= b; i++) {
            System.out.println("Pertenece al listado el numero " + i);
            conSum = conSum + i;
        }
        System.out.println("La suma del listado es " + conSum);
    }

    public static void listMult() {
        int n, mulSiete;
        Scanner mul = new Scanner(System.in);

        System.out.println("\nEjercicio 4");
        System.out.println("Ingrese el numero de multiplos que desea saber de 7");
        n = mul.nextInt();
        for (int i = 1; i <= n; i++) {
            mulSiete = 7 * i;
            System.out.println("Es multiplo de 7 el número " + mulSiete);
        }
    }

    public static void sumNum() {
        int n, sumPar = 0;
        Scanner sum = new Scanner(System.in);

        System.out.println("\nEjercicio 5");
        System.out.println("Ingrese cinco (5) pares de numeros");

        for (int i = 1; i <= 10; i++) {
            System.out.println("\nIngrese un numero");
            n = sum.nextInt();
            sumPar = sumPar + i;
        }
        System.out.println("La suma de los 5 pares de numeros es " + sumPar);
    }

    public static void rerSem() {
        int recorrido, totRec = 0;
        Scanner rec = new Scanner(System.in);
        System.out.println("\nEjercicio 1");

        for (int i = 1; i <= 7; i++) {
            System.out.println("Ingrese la distancia recorrida del dia " + i);
            recorrido = rec.nextInt();
            totRec = totRec + recorrido;
        }
        System.out.println("\nLa distancia recorrida en la semana fue " + totRec);

        if (totRec >= 100) {
            System.out.println("Logro el objetivo de la semana");
        } else {
            System.out.println("No logro el objetivo de la semana");
        }
    }

    public static void promNotas() {
        double nota, totNotas = 0;
        Scanner not = new Scanner(System.in);
        System.out.println("\nEjercicio 2");

        for (int i = 1; i <= 6; i++) {
            System.out.println("Ingrese las " + i + " nota obtenida");
            nota = not.nextInt();
            totNotas = totNotas + nota;
        }
        System.out.println("El promedio del semestre es " + totNotas / 6);
    }

    public static void venTotales() {
        int venta, valVenta, totVenta = 0;
        Scanner ven = new Scanner(System.in);
        System.out.println("\nEjercicio 3");

        System.out.println("Ingrese el numero de ventas realizadas del producto");
        venta = ven.nextInt();
        for (int i = 0; i < venta; i++) {
            System.out.println("Ingrese el valor de la venta indicada");
            valVenta = ven.nextInt();
            totVenta = totVenta + valVenta;
        }
        System.out.println("\nEl total de ventas es: $" + totVenta + " y el promedio es: $" + totVenta / venta);
    }
}
